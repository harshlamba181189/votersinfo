const functions = require("firebase-functions");
const express = require('express');
const engines = require('consolidate');
const admin = require('firebase-admin');
var serviceAccount = require("./voters-info-ea98b-firebase-adminsdk-ibn6f-c5245ed80b.json");

const app = express();

app.engine('hbs', engines.handlebars);
app.set('views', './views');
app.set('view engine', 'hbs');

app.use(function(req, res, next){
    res.setHeader('Content-Type', 'application/json');
    next();
});

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://voters-info-ea98b.asia-south1.firebasedatabase.app'
});

async function getFirestore() {
    const firestore_con = await admin.firestore();
    const collectionRef = firestore_con.collection('users');

    return collectionRef.listDocuments().then(documnetRefs => {
        return firestore_con.getAll(...documnetRefs);
    }).then(documentSnapshots=> {
        const data = [];

        for (let documentSnapshot of documentSnapshots){
            if(documentSnapshot.exists){
                data.push(documentSnapshot.data());
            } else {
                console.log('No data');
            }
        }

        return data;
    });
}

app.get('/', async (request, response) => {
    var db_result = await getFirestore();
    response.send({ db_result });
});

exports.app = functions.https.onRequest(app);

// // Create and deploy your first functions
// // https://firebase.google.com/docs/functions/get-started
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
